package files;

public class Flower implements Comparable<Flower>{
	private String name;
	private int length;
	
	public Flower(String name, int length) {
		this.name = name;
		this.length = length;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Flower && 
				((Flower)obj).name == name && 
				((Flower)obj).length == length)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return length + name.hashCode();
	}
	
	@Override
	public int compareTo(Flower f) {
		return Integer.compare(name.length()+length,f.length + f.name.length() );
	}
}

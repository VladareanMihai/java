package files;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;


class SortedList<E> extends AbstractList<E> {

    private List<E> internalList = new ArrayList<E>();

    @Override 
    public void add(int position, E e) {
        internalList.add(e);
        Collections.sort(internalList, null);
    }
    @Override 
    public boolean add(E e) {
    	if(internalList.contains(e))
    	{
    		return false;
    	}
        internalList.add(e);
        Collections.sort(internalList, null);
		return true;
    }
    @Override
    public E get(int i) {
        return internalList.get(i);
    }

    @Override
    public int size() {
        return internalList.size();
    }
}

class ReverseSortedList<E> extends AbstractList<E> {
    private List<E> internalList = new ArrayList<E>();

    @Override 
    public void add(int position, E e) {
        internalList.add(e);
        Collections.sort(internalList, Collections.reverseOrder());
    }
    @Override 
    public boolean add(E e) {
    	if(internalList.contains(e))
    	{
    		return false;
    	}
        internalList.add(e);
        Collections.sort(internalList, Collections.reverseOrder());
		return true;
    }
	
	public Comparator<E> comparator() {
		return Collections.reverseOrder();
	};
	
	 @Override
    public E get(int i) {
        return internalList.get(i);
    }

    @Override
    public int size() {
        return internalList.size();
    }
}

public class FlowerCollection {
	private List<Collection<Flower>> collections = Arrays.asList(
			new HashSet<Flower>(),
			new SortedList<Flower>(),
			new ReverseSortedList<Flower>()
			);

	public void add(Flower fl) {
		for (Collection<Flower> set : collections) {
			set.add(fl);
		}
	}

	public HashSet<Flower> getHashSet() {
		// TODO: change this to your needs
		return (HashSet<Flower>) collections.get(0);
	}

	public List<Flower> getSortedList() {
		// TODO: change this to your needs
		return (List<Flower>) collections.get(1);
	}

	public List<Flower> getReverseSortedList() {
		// TODO: change this to your needs
		return (List<Flower>) collections.get(2);
	}

	public Comparator<Flower> getReverseFlowerComparator() {
		// TODO: change this to your needs
		return ((ReverseSortedList<Flower>) collections.get(2)).comparator();
	}

}

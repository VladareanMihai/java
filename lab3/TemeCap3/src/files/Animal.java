package files;

import java.util.List;

public class Animal {
	protected int value;
	
	public Animal() {
		value = 1;
	}
	
	public int getValue() { return value; }
	
	static public void addAnimal(List<? super Animal> animals, Animal animal) {
		animals.add(animal);
	}
	
	static public void addCat(List<? super Cat> animals, Cat cat) {
		animals.add(cat);
	}
	
	static public void addFish(List<? super Fish> animals, Fish fish) {
		animals.add(fish);
	}
	
	static public int sum(List<? extends Animal> animals) {
		int sum = 0;
		for (Animal animal : animals) {
			sum += animal.value;
		}
		return sum;
	}

}

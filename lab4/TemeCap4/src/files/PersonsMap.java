package files;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class PersonsMap {
	private Map<String, Integer> internalMap = new HashMap<String, Integer>();
	
	public Map<String, Integer> getMap() {
		return internalMap;
	}

	public Function<String, Integer> getMapperCreator() {
		return k -> k.length();
	}

	public BiFunction<String, Integer, Integer> getMapperModifier() {
		// TODO Auto-generated method stub
		return (k,v) -> v == 6 ? null: (int)k.toLowerCase().charAt(0);
	}

}

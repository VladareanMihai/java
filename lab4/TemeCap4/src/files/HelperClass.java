package files;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import tests.DiverseTest;
import tests.DiverseTest.PrivateClass;

public class HelperClass {

	public static void callSetWithReflection(DiverseTest.PrivateClass pc) {
		try {
			Method set = pc.getClass().getDeclaredMethod("set", null);
			set.setAccessible(true);
			set.invoke(pc, null);
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void modifyWithReflection(PrivateClass pc) {
		try {
			Field id = pc.getClass().getDeclaredField("id");
			id.setAccessible(true);
			id.set(pc, 5);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

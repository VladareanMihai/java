package files;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MyCollection {

	private List<Dog> list = new ArrayList<Dog>();
	public Supplier<List<Dog>> getSupplier() {
		// TODO Auto-generated method stub
		return new Supplier<List<Dog>>(){

			@Override
			public List<Dog> get() {
				// TODO Auto-generated method stub
				return list;
			}
			
		};
	}

	public Consumer<List<Dog>> getSorter() {
		// TODO Auto-generated method stub
		return l->l.sort((d1,d2)->d1.getId() - d2.getId());
	}

	public void populate() {
		int i=0;
		Collections.addAll(list, new Dog(++i),new Dog(++i),new Dog(++i),new Dog(++i));

	}

	public void filter(Predicate<Dog> evenDogs) {
		list.removeIf(d -> !evenDogs.test(d));
		
	}

}

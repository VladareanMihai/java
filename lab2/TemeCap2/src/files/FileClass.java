package files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileClass {
	private File file;
	
	public FileClass(String string) {
		file = new File(string);
	}

	public File getFile() {
		// TODO Auto-generated method stub
		return file;
	}

	public void write(String string) {
		try {
			FileWriter fw = new FileWriter(file);
			fw.write(string);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clean() {
		file.delete();
	}
}

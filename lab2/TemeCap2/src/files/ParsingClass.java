package files;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class ParsingClass {
	private String artist, name, xml;
	private List<String> tracks;
	public ParsingClass(String string) {
		tracks = new ArrayList<String>();
		xml = string;
	}
	
	public void parse() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
	    try {
			 builder = factory.newDocumentBuilder();
		     Document doc = builder.parse(new InputSource(new StringReader(xml)));
		     artist = doc.getElementsByTagName("artist").item(0).getTextContent();
		     name = doc.getElementsByTagName("name").item(0).getTextContent();
		     NodeList nl = doc.getElementsByTagName("track");
		     for(int i=0;i<nl.getLength();i++)
		     {
		    	 tracks.add(nl.item(i).getTextContent());
		     }
		    
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}

	public Object getArtist() {
		return artist;
	}

	public Object getName() {
		return name;
	}

	public List<String> getTracks() {
		return tracks;
	}

}

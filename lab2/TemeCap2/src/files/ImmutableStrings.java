package files;

public class ImmutableStrings {
	private String string;
	
	public ImmutableStrings(String value) {
		string = value;
	}
	
	public void add(String val) {
		string = string.concat(val);
	}
	
	public String getValue() {
		// ...
		return string;
	}

	public void setTo(String string, boolean check) {
		if(check)
			this.string = string;
		else
			this.string = this.string.replaceAll(this.string, string);
	}
}

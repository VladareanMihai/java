package files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MyWriter {
	private String dir = "myDir", file = "bar.txt";
	private FileWriter fw;
	
	public MyWriter()
	{
		try {
			new File(dir).mkdir();
			fw = new FileWriter(dir + "/" + file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
			
	public int write(String string) {
		try {
			fw.append(string);
			fw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return string.length();
	}

	public void close() {
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void remove() {
		new File(dir + "/" + file).delete();
		new File(dir).delete();
		
	}

}

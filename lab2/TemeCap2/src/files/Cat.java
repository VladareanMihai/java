package files;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Animal {

	protected transient int legs;
	protected String name;
}

public class Cat extends Animal implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Cat() {
		super();
		legs = 4; 
	}
	public void setLegs(int i) {
		legs = i;
	}

	public void setName(String string) {
		name = string;
	}

	public int getLegs() {
		return legs;
	}

	public Object getName() {
		return name;
	}
	
	private void writeObject(ObjectOutputStream os) throws IOException {
		try
		{
			os.writeUTF(name);
			os.writeInt(4);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void readObject(ObjectInputStream is) throws ClassNotFoundException, IOException {
		try
		{
			name = is.readUTF();
			legs = is.readInt();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

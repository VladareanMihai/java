package ro.unitbv.mi.javaweb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	
	@Column(name = "firstName", nullable = false, length = 30)
    private String firstName;
	
	@Column(name = "lastName", nullable = false, length = 30)
    private String lastName;
	
	@Column(name = "age", nullable = false)
    private int age;
	
	@Column(name = "address", nullable = false, length = 100)
    private String address;
    
	public Person() { }
	public Person(String firstName, String lastName, int age, String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
				+ ", address=" + address + "]";
	}

}

package ro.unitbv.mi.javaweb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "nota")
public class Nota {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	
	@Column(name = "nume", nullable = false, length = 30)
    private String nume;
	
	@Column(name = "nota", nullable = false)
    private int nota;
	
	public Nota() { }
	public Nota(String nume, int nota) {
		this.nume = nume;
		this.nota = nota;
	}

	@Override
	public String toString() {
		return "Nota [id=" + id + ", nota=" + nota + ", nota=" + nota;
	}

}

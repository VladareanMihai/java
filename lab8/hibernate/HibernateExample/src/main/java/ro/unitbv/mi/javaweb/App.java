package ro.unitbv.mi.javaweb;

import java.util.List;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class App 
{    
    public static void main( String[] args )
    {
    	SessionFactory sessionFactory = HibernateConfig.configureSessionFactory();
    	
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        Person p1 = new Person("Radu", "Popescu", 33, "Florilor 50");
	        Person p2 = new Person("Vasile", "Ionescu", 28, "Macaralei 12");
	        
			sess.save(p1);
			sess.save(p2);
			
			sess.flush();
			sess.getTransaction().commit();
			
			// fetch saved data
			List<Person> persList = sess.createQuery("from Person").list();	       
	        //persList = sess.createQuery("from Person where id=1").list();
	        
	        for (Person p: persList)
	        	System.out.println(p);
	        
	        Nota n1 = new Nota("Radu",9);
	        Nota n2 = new Nota("Vasile",8);
	        
			sess.save(n1);
			sess.save(n2);
			
			sess.flush();
			sess.getTransaction().commit();
			
			// fetch saved data
			List<Nota> notaList = sess.createQuery("from Nota").list();	       
	        //persList = sess.createQuery("from Person where id=1").list();
	        
	        for (Nota n: notaList)
	        	System.out.println(n);
	        
	        
        } catch (HibernateException he) {
        	he.printStackTrace();
        	
        	sess.getTransaction().rollback();
        } finally {
        	sess.close();
        }
        
        sessionFactory.close();
    }
}

package ro.unitbv.mi.javaweb;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{
	private SessionFactory sessionFactory = null;
	
	public void setUp() {
		sessionFactory = HibernateConfig.configureSessionFactory();
	}
			
    public AppTest( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testHibernatePut()
    {    	
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        Person p1 = new Person("Radu", "Popescu", 33, "Florilor 50");
	        Person p2 = new Person("Vasile", "Ionescu", 28, "Macaralei 12");
	        
	        sess.save(p1);
	        sess.save(p2);
	        
	        sess.flush();
	        sess.getTransaction().commit();	 
	        
	        List<Person> persList = sess.createQuery("from Person").list();
	        assertTrue(persList.size() > 0);
	        
	        persList = sess.createQuery("from Person where id=1").list();
	        assertTrue(persList.size() == 1);	        
        } catch (HibernateException he) {
        	he.printStackTrace();
        	
        	sess.getTransaction().rollback();
        } finally {
        	sess.close();
        }        
        
        assertTrue(true);
    }
    	    
    public void testHibernateDelete()
    {
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        int deletedEntries = sess.createQuery("delete Person").executeUpdate();
	        assertTrue(deletedEntries > 0);
	        
	        sess.flush();
	        sess.getTransaction().commit();
        } catch (HibernateException he) {
        	he.printStackTrace();        
        } finally {
        	sess.close();
        }
        
        assertTrue(true);
    }
    
    public void testHibernatePutNota()
    {    	
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        Nota n1 = new Nota("Radu",9);
	        Nota n2 = new Nota("Vasile",8);
	        
			sess.save(n1);
			sess.save(n2);
	        
	        sess.flush();
	        sess.getTransaction().commit();	 
	        
	        List<Person> notaList = sess.createQuery("from Nota").list();
	        assertTrue(notaList.size() > 0);
	        
	        notaList = sess.createQuery("from Nota where id=1").list();
	        assertTrue(notaList.size() == 1);	        
        } catch (HibernateException he) {
        	he.printStackTrace();
        	
        	sess.getTransaction().rollback();
        } finally {
        	sess.close();
        }        
        
        assertTrue(true);
    }
    	    
    public void testHibernateDeleteNota()
    {
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        int deletedEntries = sess.createQuery("delete Nota").executeUpdate();
	        assertTrue(deletedEntries > 0);
	        
	        sess.flush();
	        sess.getTransaction().commit();
        } catch (HibernateException he) {
        	he.printStackTrace();        
        } finally {
        	sess.close();
        }
        
        assertTrue(true);
    }
    
    
}

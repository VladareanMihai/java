package ro.unitbv.mi.javaweb.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import ro.unitbv.mi.javaweb.model.Person;
import ro.unitbv.mi.javaweb.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	private List<Person> persons = new ArrayList<Person>();
	private AtomicLong idGenerator = new AtomicLong(0);

	public List<Person> listPersons() {
		synchronized (persons) {
			return new ArrayList<Person>(persons);
		}
	}

	public long addPerson(Person person) {
		synchronized (persons) {
			Person clonedPerson = new Person(person);
			clonedPerson.setId(idGenerator.incrementAndGet());
			persons.add(clonedPerson);
			return clonedPerson.getId();
		}
	}

	public boolean removePerson(long id) {
		synchronized (persons) {
			return persons.removeIf( p -> p.getId() == id);
		}
	}

	public void close() throws IOException {
		synchronized (persons) {
			persons.clear();
		}
	}

}

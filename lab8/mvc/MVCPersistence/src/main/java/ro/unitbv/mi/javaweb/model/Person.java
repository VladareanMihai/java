package ro.unitbv.mi.javaweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;
	@Column(name = "firstName", nullable = false, length = 30)
	private String firstName;
	@Column(name = "lastName", nullable = false, length = 30)
	private String lastName;
	@Column(name = "age", nullable = false)
	private int age;
	@Column(name = "address", nullable = false, length = 30)
	private String address;

	public Person(Person person) {
		setId(person.getId());
		setFirstName(person.getFirstName());
		setLastName(person.getLastName());
		setAge(person.getAge());
		setAddress(person.getAddress());
	}

	public Person(String firstName, String lastName, int age, String address) {
		setFirstName(firstName);
		setLastName(lastName);
		setAge(age);
		setAddress(address);
	}

	public Person() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
				+ ", address=" + address + "]";
	}
}

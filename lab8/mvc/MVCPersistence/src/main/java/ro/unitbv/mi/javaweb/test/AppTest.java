package ro.unitbv.mi.javaweb.test;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ro.unitbv.mi.javaweb.model.Person;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{
	private SessionFactory sessionFactory = null;
	
	public void setUp() {
		sessionFactory = HibernateConfig.configureSessionFactory();
	}
			
    public AppTest( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testHibernatePut()
    {    	
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        Person p1 = new Person("Radu", "Popescu", 33, "Florilor 50");
	        Person p2 = new Person("Vasile", "Ionescu", 28, "Macaralei 12");
	        
	        sess.save(p1);
	        sess.save(p2);
	        
	        sess.flush();
	        sess.getTransaction().commit();	 
	        
	        List<Person> persList = sess.createQuery("from Person").list();
	        assertTrue(persList.size() > 0);
	        
	        persList = sess.createQuery("from Person where id=1").list();
	        assertTrue(persList.size() == 1);	        
        } catch (HibernateException he) {
        	he.printStackTrace();
        	
        	sess.getTransaction().rollback();
        } finally {
        	sess.close();
        }        
        
        assertTrue(true);
    }
    	    
    public void testHibernateDelete()
    {
        Session sess = null;
        
        try {
	        sess = sessionFactory.openSession();
	        sess.beginTransaction();
	        
	        int deletedEntries = sess.createQuery("delete Person").executeUpdate();
	        assertTrue(deletedEntries > 0);
	        
	        sess.flush();
	        sess.getTransaction().commit();
        } catch (HibernateException he) {
        	he.printStackTrace();        
        } finally {
        	sess.close();
        }
        
        assertTrue(true);
    }
       
    
}

package ro.unitbv.mi.javaweb.service;

import java.io.Closeable;
import java.util.List;

import ro.unitbv.mi.javaweb.model.Person;

public interface PersonService extends Closeable {

  List<Person> listPersons();

  long addPerson(Person person);

  boolean removePerson(long id);
}

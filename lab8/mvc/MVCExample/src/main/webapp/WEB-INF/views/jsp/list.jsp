<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Persons</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Spring 3 MVC Project</a>
			</div>
		</div>
	</nav>

	<div class="jumbotron"></div>

	<div class="container">

		<div class="row">
			<table class="table table-striped table-bordered">
	  		<tr>
		  		<th>First name</th>
		  		<th>Last name</th>
		  		<th>Age</th>
		  		<th>Address</th>
	  		</tr>
			<c:forEach items="${personsList}" var="person">
				<tr>
				<td>${person.firstName}</td>
				<td>${person.lastName}</td>
				<td>${person.age}</td>
				<td>${person.address}</td>
				</tr>
			</c:forEach>
			</table>

			<div class="col-md-4">
				<p>
					<a class="btn btn-primary btn-lg" href="/MVCExample/persons/add" role="button">Add persons</a>
				</p>
			</div>
		</div>
		<hr>
		<footer>
			<p>List page</p>
		</footer>

	</div>

	<spring:url value="/resources/core/css/hello.js" var="coreJs" />
	<spring:url value="/resources/core/css/bootstrap.min.js"
		var="bootstrapJs" />

	<script src="${coreJs}"></script>
	<script src="${bootstrapJs}"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</body>
</html>
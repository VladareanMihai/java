<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Add a person</title>

<spring:url value="/resources/core/css/hello.css" var="coreCss" />
<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Spring 3 MVC Project</a>
			</div>
		</div>
	</nav>

	<div class="jumbotron"></div>

	<div class="container">

		<div class="row">
			<table class="table table-striped table-bordered">
	  		<tr>
		  		<th>First name</th>
		  		<th>Last name</th>
		  		<th>Age</th>
		  		<th>Address</th>
	  		</tr>
			<c:forEach items="${personsList}" var="person">
				<tr>
				<td>${person.firstName}</td>
				<td>${person.lastName}</td>
				<td>${person.age}</td>
				<td>${person.address}</td>
				</tr>
			</c:forEach>
			</table>

			<form:form method="POST" modelAttribute="person">
				<p>
					First Name: <input type="text" name="firstName" value="${person.firstName}" />
				</p>
				<p>
					Last Name: <input type="text" name="lastName" value="${person.lastName}" />
				</p>
				<p>
					Address: <input type="text" name="address" value="${person.address}" />
				</p>
				<p>
					Age: <input type="text" name="age" value="${car.age}" />
				</p>
				<!-- <p>
					<input type="hidden" name="op" value="0">
					Price: <input type="button" value="+" onclick="document.forms[0].op.value='+'; document.forms[0].submit();" />
				</p> -->
				
				
				
				<div class="col-md-4">
				<p>
					<input type="submit" class="btn btn-primary btn-lg" value="Add to list" />
					<a class="btn btn-primary btn-lg" href="/MVCExample/persons/list" role="button">List</a>
				</p>
				</div>
			</form:form>

		</div>
		
		<hr>
		<footer>
			<p>Add page</p>
		</footer>

	</div>

	<spring:url value="/resources/core/css/hello.js" var="coreJs" />
	<spring:url value="/resources/core/css/bootstrap.min.js"
		var="bootstrapJs" />

	<script src="${coreJs}"></script>
	<script src="${bootstrapJs}"></script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</body>
</html>
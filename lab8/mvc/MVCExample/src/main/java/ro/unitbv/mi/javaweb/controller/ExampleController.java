package ro.unitbv.mi.javaweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ro.unitbv.mi.javaweb.model.Person;
import ro.unitbv.mi.javaweb.service.PersonService;

@Controller
public class ExampleController {
	@Autowired
	private PersonService personService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Spring 3 MVC Hello World");
		return "hello";

	}

	@RequestMapping(value = "/hello/{name:.+}", method = RequestMethod.GET)
	public ModelAndView hello(@PathVariable("name") String name) {
		ModelAndView model = new ModelAndView();
		model.setViewName("hello");
		model.addObject("msg", name);

		return model;
	}

	@RequestMapping("/persons/list")
	public String personsList(Model model) {
		List<Person> personsList = personService.listPersons();
		model.addAttribute("personsList", personsList);

		return "list";
	}
	
	@RequestMapping(value = "/persons/listall")
	public @ResponseBody List<Person> personsListAll() {
		return personService.listPersons();
	}

	@RequestMapping("/persons/add")
	public ModelAndView personAdd() {
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("");
		person.setAddress("");
		person.setAge(0);
		person.setId(0);

		ModelAndView model = new ModelAndView();
		model.setViewName("add");
		model.addObject("person", person);
		
		List<Person> personsList = personService.listPersons();
		model.addObject("personsList", personsList);

		return model;
	}

	@RequestMapping(value = "/persons/add", method = RequestMethod.POST)
	public String personAddSubmit(@ModelAttribute Person person) {
		personService.addPerson(person);
		return "redirect:/persons/list";
	}

}

Tema
====

1) Rescrieti metoda ro.unitbv.mi.javaweb.service.impl.PersonServiceImpl.removePerson() folosind
   predicate si apelul Java 8 removeIf().

2) Pentru proiectul Hibernate, adaugati o alta entitate, Nota(id, nume, nota), unde "Nota" este numele
   entitatii (si deci numele tabelei), "id" este un intreg, identificator auto-generat, "nume" este un
   string de maxim 30 iar nota tot un intreg. Realizati si testele care verifica scrierea/citirea/stergerea.

3) Proiectul MVCPersistence nu are o clasa person persistenta si dupa terminarea aplicatiei. Modificati
   codul in asa fel incat sa adaugati structurile de tip Hibernate necesare. Incepeti de la adaugarea
   dependintelor in pom.xml. Adaugati si testele specifice.

4) Pentru proiectul MVCExample, adaugati cod in asa fel incat sa puteti modifica orice atribut (in afara
   de id) pentru o inregistrare de tip Person. Pentru aceasta, adaugati la lista generata de persoane un buton.
   La apasarea acelui buton veti construi alt view ce va permite editarea inregistrarii. Edit box-urile vor fi
   initial populate cu vechile valori.

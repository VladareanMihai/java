package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import blocks.*;

public class DiverseTest {

	@Test
	public void testCreation() {
		assertEquals(InitBlocks.counter, 1);
		
		InitBlocks b = new InitBlocks();
		
		assertTrue(b != null);
		assertEquals(InitBlocks.counter, 4);
	}
	
	@Test
	public void testAutoClose() {		
		boolean exceptionThrown = false;
		
		try (CloseableResource res = new CloseableResource(2)) {
			assertTrue(res != null);
			
			res.use();
		} catch (Exception e) {
			assertTrue(CloseableResource.closed == false);
			e.printStackTrace();
			
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
		
		try (CloseableResource res = new CloseableResource(3)) {
			assertTrue(res != null);
			
			res.use();
		} catch (Exception e) {
			fail("this code should not be reached");
			e.printStackTrace();
		} finally {
			assertTrue(CloseableResource.closed == true);
		}
	}
	
	@Test
	public void testSingletons() {
		Singleton1 s11 = Singleton1.getInstance();
		Singleton1 s12 = Singleton1.getInstance();
		assertTrue(s11 == s12);
		
		Singleton2 s21 = Singleton2.getInstance();
		Singleton2 s22 = Singleton2.getInstance();
		assertTrue(s21 == s22);
		
		Singleton3 s31 = Singleton3.getInstance();
		Singleton3 s32 = Singleton3.getInstance();
		assertTrue(s31 == s32);
	}
	
	@Test
	public void testTiles() {
		//Tile t = new Tile();
		//Tile t = new TileFactory.TileImpl();
		
		TileFactory tf = new TileFactory();
		
		Tile tg = tf.newTile(Tile.Color.GREEN);
		assertEquals(tg.getColor(), "green");
		
		Tile tr = tf.newTile(Tile.Color.RED);
		assertEquals(tr.getColor(), "red");
		
		assert(tr.getColor() != "green"); // how can this be true ?!
		
		// TODO: realizati doua clase factory pentru Tile, una pentru tigle verzi
		// si alta pentru tigle rosii. Cele doua clase le veti implementa ca si clase
		// de tip singleton, ce mostenesc o interfata comuna cu o singura metoda:
		//
		// interface TileFactory {
		//		public Tile newTile();
		// }
		//
		// Realizati unit test-ul ce testeaza crearea celor doua clase, diferite,
		// ce creeaza tigle rosii si respectiv verzi. Verificati ca cele doua clase
		// factory sunt singleton dar ca tiglele create sunt obiecte diferite intre ele.
	}
	
	// TODO: realizati o clasa factory pentru Tile, pentru tigle verzi
	// si rosii. Factory-ul va intoarce doar doua obiecte, tile red sau green.
	// De exemplu prima data se creeaza un tile verde. La al doilea apel pentru tile
	// verde, se va returna primul obiect intors mai devreme.
	@Test
	public void testSingletonTiles() {		
		TileFactoryS tf = new TileFactoryS();
		
		Tile tg1 = tf.newTile(Tile.Color.GREEN);
		assertEquals(tg1.getColor(), "green");
		
		Tile tg2 = tf.newTile(Tile.Color.GREEN);
		assertEquals(tg2.getColor(), "green");
		
		assertTrue(tg1 == tg2);
		
		Tile tr1 = tf.newTile(Tile.Color.RED);
		assertEquals(tr1.getColor(), "red");
		
		Tile tr2 = tf.newTile(Tile.Color.RED);
		assertEquals(tr2.getColor(), "red");
		
		assertTrue(tr1 == tr2);
		
		assertTrue(tg1 != tr1);
	}
	
	@Test
	public void testSemaphoreEnum() {
	// TODO: decomentati aceste linii !
		assertEquals(SemaphoreEnum.values().length, 3);
		assertEquals(SemaphoreEnum.values()[0], SemaphoreEnum.RED); 
		assertEquals(SemaphoreEnum.values()[1], SemaphoreEnum.YELLOW);
		assertEquals(SemaphoreEnum.values()[2], SemaphoreEnum.GREEN);
		
		assertEquals(SemaphoreEnum.RED.getValue(), 3);
		assertEquals(SemaphoreEnum.YELLOW.getValue(), 2);
		assertEquals(SemaphoreEnum.GREEN.getValue(), 1);
		
		assertEquals(SemaphoreEnum.RED.isSafeToEnterCrossing(), false);
		assertEquals(SemaphoreEnum.YELLOW.isSafeToEnterCrossing(), true);
		assertEquals(SemaphoreEnum.GREEN.isSafeToEnterCrossing(), true);
	}
	
	@Test
	public void testException() {

		int s = 0;
		ExceptionThrowerClass etc = new ExceptionThrowerClass();
		try {
			etc.method(3);
		} catch (IllegalArgumentException e) {
			s += 1;
		} catch (RuntimeException e) {
			s += 2;
		} catch(Exception e) {
			s += 4;
		}
		assertTrue(s == 0);
		
		s = 0;
		try {
			etc.method(2);
		} catch (IllegalArgumentException e) {
			s += 1;
		} catch (RuntimeException e) {
			s += 2;
		} catch(Exception e) {
			s += 4;
		}
		assertTrue(s == 1);
		
		s = 0;
		try {
			etc.method(1);
		} catch (IllegalArgumentException e) {
			s += 1;
		} catch (RuntimeException e) {
			s += 2;
		} catch(Exception e) {
			s += 4;
		}
		assertTrue(s == 2);
		
		s = 0;
		try {
			etc.method(0);
		} catch (IllegalArgumentException e) {
			s += 1;
		} catch (RuntimeException e) {
			s += 2;
		} catch(Exception e) {
			s += 4;
		}
		assertTrue(s == 4);
	}
	
	// (1) Realizati implementarea acestui factory folosind de data aceasta
	// o clasa anonima pentru implementarea interna a interfetei Tile
	// (2) Realizati un nou test testSingletonTileFactoryA() dupa modelul
	// testSingletonTiles() si scrieti acel factory (*)
	// (3) Extindeti enumerarea Tile.Color la 9 culori (Alb, Rosu, Orange,
	// Galben, Verde, Albastru, Indigo, Violet, Negru), si realizati
	// factory-ul de asa natura incat sa intoarca doar o instanta de tile
	// pentru fiecare din cele 9 culori. Folositi un HashMap ca sa nu scrieti
	// cod duplicat. (**)
	@Test
	public void testTileFactoryA() {
		TileFactoryA tf = new TileFactoryA();
		
		Tile tg = tf.newTile(Tile.Color.GREEN);
		assertEquals(tg.getColor(), "green");
		
		Tile tr = tf.newTile(Tile.Color.RED);
		assertEquals(tr.getColor(), "red");
	}
	
	@Test
	public void testSingletonTileFactoryA(){
		TileSingletonFactoryA tf = new TileSingletonFactoryA();
		
		Tile tg = tf.newTile(Tile.Color.GREEN);
		assertEquals(tg.getColor(), "green");
		
		Tile tr = tf.newTile(Tile.Color.RED);
		assertEquals(tr.getColor(), "red");

		Tile tb = tf.newTile(Tile.Color.BLUE);
		assertEquals(tb.getColor(), "blue");
		
		Tile tblack = tf.newTile(Tile.Color.BLACK);
		assertEquals(tblack.getColor(), "black");
		
		Tile tblack1 = tf.newTile(Tile.Color.BLACK);
		assertTrue(tblack == tblack1);
	}
}

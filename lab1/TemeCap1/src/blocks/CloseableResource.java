package blocks;

public class CloseableResource implements AutoCloseable {
	public static boolean closed;
	private int n;
	public CloseableResource(int n) {
		this.n = n;
	}
	
	public void use () throws Exception {
		closed = false;
	}

	@Override
	public void close() throws Exception {
		if(n==2)
			throw new Exception();
		closed = true;
	}

}

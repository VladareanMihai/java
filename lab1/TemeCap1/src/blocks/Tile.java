package blocks;

public interface Tile {
	public String getColor();
	
	enum Color { 
		GREEN,
		RED,
		WHITE,
		ORANGE,
		YELLOW,
		BLUE,
		INDIGO,
		VIOLET,
		BLACK
		};
}

package blocks;

import java.util.HashMap;

public class TileSingletonFactoryA {
	
	private HashMap<Tile.Color,Tile> instances;
	public TileSingletonFactoryA()
	{
		instances = new HashMap<>();
	}
	public Tile newTile(Tile.Color color) {
		Tile tile = instances.get(color);
		if(tile == null)
		{
			instances.put(color, new Tile(){
				@Override
				public String getColor() {
					return color.toString().toLowerCase();
				}
			});
		}
		return instances.get(color);
	}
}
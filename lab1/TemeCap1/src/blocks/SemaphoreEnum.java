package blocks;

public enum SemaphoreEnum {
	RED,
	YELLOW,
	GREEN;

	public Object getValue() {
		// TODO Auto-generated method stub
		return values().length - this.ordinal();
	}

	public Object isSafeToEnterCrossing() {
		// TODO Auto-generated method stub
		if(this == RED)
			return false;
		return true;
	}
}

package blocks;

public class InitBlocks {
    public static int counter;
    
    // static initialization block
    static {
    	counter = 1;
    }

    // initialization block
    {
    	
    }

    public InitBlocks() {
         counter *= 4 ;
    }
    
}

package blocks;
public class TileFactoryS extends TileFactory{
	
	// TODO: your code here
	// ...
	private static Tile instanceGreen, instanceRed;
	public Tile newTile(Tile.Color color) {
		if(color == Tile.Color.GREEN)
		{
			if(instanceGreen == null)
				instanceGreen = super.newTile(color);
			return instanceGreen;
		}
		else if(color == Tile.Color.RED)
		{
			if(instanceRed == null)
				instanceRed = super.newTile(color);
			return instanceRed;
		}
		else return null;
			
	}

}

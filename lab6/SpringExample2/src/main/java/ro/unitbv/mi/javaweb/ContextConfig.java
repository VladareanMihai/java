package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ContextConfig {
	
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Bean
	@Scope("prototype")
	public Foo foo() {
		log.info("creating component Foo");
		
		return new Foo();
	}

}

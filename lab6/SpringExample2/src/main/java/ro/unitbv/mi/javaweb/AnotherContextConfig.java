package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AnotherContextConfig {
	
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Bean
	@Scope("singleton")
	public Bar bar() {
		log.info("creating component Bar");
		
		return new Bar();
	}
	
	@Bean
	@Scope("prototype")
	public Baz baz() {
		log.info("creating component Baz");
		
		return new Baz();
	}
	
	

}

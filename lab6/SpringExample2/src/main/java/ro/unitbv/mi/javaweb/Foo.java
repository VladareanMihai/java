package ro.unitbv.mi.javaweb;

public class Foo {
	private String message = "default";

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
    public AppTest( String testName ) {
        super( testName );
    }

    public static Test suite() {
        return new TestSuite( AppTest.class );
    }

    public void testApp() {
    	ApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);
    	
    	Foo foo = context.getBean(Foo.class);
    	log.info("foo: " + foo.getMessage());
    	
    	((AnnotationConfigApplicationContext)context).close();
    	
        assertTrue( true );
    }
    
    // TODO: decomentati liniile de mai jos si
    // scrieti codul necesar pentru ca testul sa treaca!
    public void testDiverse() {
    	ApplicationContext context = null;
    	try {
    		context = new AnnotationConfigApplicationContext(AnotherContextConfig.class);
	    	
	    	Bar bar1 = (Bar)context.getBean(Bar.class);
	    	Bar bar2 = (Bar)context.getBean(Bar.class);
	    	assertTrue(bar1.getName().equals("default"));
	    	assertTrue(bar1 == bar2);
	    	
	    	Baz baz1 = (Baz)context.getBean(Baz.class);
	    	Baz baz2 = (Baz)context.getBean(Baz.class);
	    	assertTrue(baz1.getName().equals("robot"));
	    	baz1.setName("altceva");
	    	assertTrue(baz1.getName().equals(baz2.getName()));
	    	assertTrue(baz1 != baz2);    	
    	} finally {
    		if (context != null)
    			((AnnotationConfigApplicationContext)context).close();
    	}
    }
}

package ro.unitbv.mi.javaweb;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/context.xml")
public class AppTest 
{
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	private HelloWorld hw;
	
    @Autowired
    @Qualifier("myhook")	// TODO: comment this line, see what happens; read the cause of error
    private Foo foo;
    
    @Autowired
    @Qualifier("fooProvider")
    private Foo foo2;
	
    @Test
	public void testApp() {
    	log.info("message: " + hw.getMessage());
        assertTrue( true );
    }
    
    @Test
	public void testFoo() {
    	log.info("message from foo: " + foo.getMessage());
    	log.info("message from foo2: " + foo2.getMessage());
        assertTrue( true );
    }
    

    // TODO: decomentati liniile de mai jos si
    // scrieti codul necesar pentru ca testul sa treaca!    
    
    @Autowired
    Bar bar1;
    
    @Autowired
    Bar bar2;
    
    @Autowired
    Baz baz1;
    
    @Autowired
    Baz baz2;
    
    @Autowired
    Animal animal;
        
    @Test
    public void testDiverse() {
    	assertTrue(bar1.getName().equals("default"));
    	assertTrue(bar1 == bar2);
    	
    	assertTrue(baz1.getName().equals("robot"));
    	baz1.setName("altceva");
    	assertTrue(baz1.getName().equals(baz2.getName()));
    	assertTrue(baz1 != baz2);    	
    	
//	 realizati un fisier animal.properties cu urmatoarele proprietati:
//	 tip=pisica
//	 picioare=4
//	 prinde=soareci
//	 populati cu aceste proprietati un bean Spring denumit Animal:

    assertTrue(animal.getTip().equals("pisica"));
    assertTrue(animal.getPicioare() == 4);
    assertTrue(animal.getPrinde().equals("soareci"));
    }
}

package ro.unitbv.mi.javaweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.core.env.Environment;

@Component
@PropertySource("classpath:/animal.properties")
public class Animal {

	@Autowired
	Environment env;
	
	public Object getTip() {
		// TODO Auto-generated method stub
		return env.getProperty("tip");
	}
	public int getPicioare() {
		// TODO Auto-generated method stub
		return Integer.parseInt(env.getProperty("picioare"));
	}
	public Object getPrinde() {
		// TODO Auto-generated method stub
		return env.getProperty("prinde");
	}
}

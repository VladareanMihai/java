package ro.unitbv.mi.javaweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/hello.properties")
public class HelloConfig {

	@Autowired
	Environment env;
	
	@Bean
	public Foo fooProvider() {
		return new Foo() {
			public String getMessage() {
				return env.getProperty("message");
			}

			public void setMessage(String msg) {
			}
		};
	}
}

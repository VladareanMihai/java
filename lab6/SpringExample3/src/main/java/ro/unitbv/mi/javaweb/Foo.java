package ro.unitbv.mi.javaweb;

public interface Foo {
	String getMessage();
	void setMessage(String msg);
}

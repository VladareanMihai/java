package ro.unitbv.mi.javaweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("myhook") // TODO: try to remove the parentheses entirely, then adjust the @Qualifier in AppTest by observing the error message
public class FooImpl implements Foo {
	private String message = "default Foo message";
	
	//TODO: observe dependency injection
	@Autowired
	Cat cat;

	public String getMessage() {
		return message + " and cat " + cat.getMessage();
	}

	public void setMessage(String msg) {
		message = msg;
	}
}

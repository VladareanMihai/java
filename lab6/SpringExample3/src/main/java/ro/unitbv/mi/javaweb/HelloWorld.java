package ro.unitbv.mi.javaweb;

import org.springframework.stereotype.Component;

@Component
public class HelloWorld implements Foo {
	private String message = "Hello World!";
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

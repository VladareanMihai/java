package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class ContextConfig {
	
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Bean
	@Scope("prototype")
	public Bar bar() {
		log.info("creating component Bar");
		
		return new Bar();
	}

	@Bean
	@Scope("prototype")
	public Baz baz() {
		log.info("creating component Baz");
		
		return new Baz();
	}
	
	@Bean
	public Animal animal() {
		return new Animal();
	}
	
}

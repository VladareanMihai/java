package ro.unitbv.mi.javaweb;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("baz")
@Scope("prototype")
public class Baz {

	private static String name = "robot";
	public Object getName() {
		// TODO Auto-generated method stub
		return name;
	}
	public void setName(String string) {
		// TODO Auto-generated method stub
		name = string;
	}
}

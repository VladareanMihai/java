package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Foo {
	private Logger log = LogManager.getLogger(Foo.class.getName());
	
	public void init() {
		log.info("in init()");
	}
	
	public void destroy() {
		log.info("in destroy()");
	}

}

package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SpellChecker {
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	public SpellChecker() {
		log.info("SpellChecker() c-tor");
	}
	
	public void spell() {
		log.info("in spell()");
	}
}

package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextEditor {
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	private SpellChecker spellChecker;
	
	public void spell() {
		log.info("in spell()");
		spellChecker.spell();
	}
	
	public void setSpellChecker(SpellChecker spellChecker) {
		log.info("in setSpellChecker()");
		this.spellChecker = spellChecker;
	}
}

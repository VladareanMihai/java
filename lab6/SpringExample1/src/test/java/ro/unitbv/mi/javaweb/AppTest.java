package ro.unitbv.mi.javaweb;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase
{
	static Logger log = LogManager.getLogger(AppTest.class.getName());
	
    public AppTest( String testName ) {
        super( testName );
    }

    public static Test suite() {
        return new TestSuite( AppTest.class );
    }

    public void testApp() {
    	ApplicationContext context = null;
    	try {
	    	context = new ClassPathXmlApplicationContext("beans.xml");
	    	
	    	// create the prototype bean
	    	HelloWorld objA = (HelloWorld)context.getBean("hw_prot");
	    	log.info("objA: " + objA.getMessage());
	    	objA.setMessage("object prototype");    	
	    	log.info("objA: " + objA.getMessage());
	    	
	    	// create another prototype bean
	    	HelloWorld objB = (HelloWorld)context.getBean("hw_prot");
	    	log.info("objB: " + objB.getMessage());
	    	
	    	assertTrue(objA != objB);
	    	
	    	// create a singleton bean
	    	HelloWorld objC = (HelloWorld)context.getBean("hw_sing");    	
	    	log.info("objC: " + objC.getMessage());
	    	
	    	// create another singleton bean
	    	HelloWorld objD = (HelloWorld)context.getBean("hw_sing");    	
	    	log.info("objD: " + objD.getMessage());
	    	
	    	assertTrue(objC == objD);
	    	
	    	Foo foo = (Foo)context.getBean("foo");    	
    	} finally {
    		if (context != null)
    			((AbstractApplicationContext) context).close();
    	}
    }
    
    // TODO: decomentati liniile de mai jos si
    // scrieti codul necesar pentru ca testul sa treaca!
    @SuppressWarnings("resource")
	public void testDiverse() {
    	ApplicationContext context = null;
    	try {
	    	context = new ClassPathXmlApplicationContext("test.xml");
	    	
	    	Bar bar1 = (Bar)context.getBean("bar");
	    	Bar bar2 = (Bar)context.getBean("bar");
	    	assertTrue(bar1.getName().equals("default"));
	    	assertTrue(bar1 == bar2);
	    	
	    	Baz baz1 = (Baz)context.getBean("baz");
	    	Baz baz2 = (Baz)context.getBean("baz");
	    	assertTrue(baz1.getName().equals("robot"));
	    	baz1.setName("altceva");
	    	assertTrue(baz1.getName().equals(baz2.getName()));
	    	assertTrue(baz1 != baz2);    	
    	} finally {
    		if (context != null)
    			((AbstractApplicationContext) context).close();
    	}
    }
}

package files;

import java.util.Deque;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FruitStack {
	//TODO: modify to your needs
	private Deque<String> stack;
	private ExecutorService service;
	private int nThread = 4;
	public FruitStack()
	{
		stack = new ConcurrentLinkedDeque<String>();
		
	}
	
	public Integer putFruits(CyclicBarrier cb) throws InterruptedException, BrokenBarrierException {
		//TODO: modify to your needs
		stack.add("mar");
		cb.await();
		stack.add("gutuie");
		cb.await();
		stack.add("pruna");
		return 3;
	}
		
	public Integer launchThreads() {
		try {
		service = Executors.newFixedThreadPool(nThread);
		CyclicBarrier cb = new CyclicBarrier(4);
		for(int i=0;i<nThread;i++)
		{
			service.submit(() -> 
					putFruits(cb)
			);
		}
		service.shutdown();
		service.awaitTermination(3, TimeUnit.SECONDS);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stack.size();
	}
	
	public Deque<String> getStack() {
		return stack;
	}
	
	public ExecutorService getExecutorService() {
		return service;
	}

}

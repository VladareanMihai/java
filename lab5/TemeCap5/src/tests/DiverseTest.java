package tests;

import static org.junit.Assert.assertTrue;
import java.util.Deque;
import org.junit.Test;
import files.FruitStack;


public class DiverseTest {
	
	// 1. Creeati patru thread-uri folosind java.util.concurrent.ExecutorService.
	// Fiecare dintre cele patru thread-uri va scrie 3 string-uri, "mar", "gutuie"
	// si "pruna" intr-o stiva. Ordinea scrierii depinde de ordinea de executie a
	// thread-urilor. Dorim ca in stiva sa se scrie intai toate merele, apoi toate
	// gutuile si apoi toate prunele. Folositi java.util.concurrent.CyclicBarrier
	// pentru a sincroniza thread-urile. Asigurati-va ca folositi o colectie
	// sincronizata.
	//
	// 2. Creeati proiectul Maven asociat cu acest exemplu, si rulati din linie de
	// comanda testele. Urmariti side-urile.
	@Test
	public void testThreads() {
		FruitStack stack = new FruitStack();
		assertTrue(stack.launchThreads() == 12);
	
		Deque<String> d = stack.getStack();
		assertTrue(d.size() == 12);
		for (int i = 0 ; i < 4 ; i ++)
			assertTrue("pruna".equals(d.pollLast()));
		for (int i = 0 ; i < 4 ; i ++)
			assertTrue("gutuie".equals(d.pollLast()));
		for (int i = 0 ; i < 4 ; i ++)
			assertTrue("mar".equals(d.pollLast()));
		
		assertTrue(stack.getExecutorService().isShutdown());
		assertTrue(stack.getExecutorService().isTerminated());
	}	
}

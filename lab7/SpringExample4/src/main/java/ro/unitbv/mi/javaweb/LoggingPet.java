package ro.unitbv.mi.javaweb;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingPet {

	@Around("execution(* ro.unitbv.mi.javaweb.Hamster.eats(..))")
	public Object checkHamsterFeeding(ProceedingJoinPoint point) throws Exception {
			
		String food = point.getArgs()[0].toString();
		Object result = null;
		
		if (food == "grain") {
			return "hamster eating grain";
		}
		
		if (food == "meat") {
			throw new IllegalArgumentException("hamster does not eat meat");
		}
		
		try {
			result = point.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return result;
	}
	@Around("execution(* ro.unitbv.mi.javaweb.Ferret.eats(..))")
	public Object checkFerretFeeding(ProceedingJoinPoint point)throws Exception  {
			
		String food = point.getArgs()[0].toString();
		Object result = null;
		
		if (food == "meat") {
			return "ferret eating meat";
		}
		if(food == "grain"){
			throw new IllegalArgumentException("ferret does not eat grain");
		}
		try {
			result = point.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return result;
	}
}

package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Cat {
	private String message = "this is a cat";
	
	private Logger log = LogManager.getLogger(this.getClass().getName());

	public String getMessage() {
		log.debug("in getMessage()");
		return message;
	}

	public void setMessage(String message) {
		log.debug("in setMessage()");
		this.message = message;
	}

}

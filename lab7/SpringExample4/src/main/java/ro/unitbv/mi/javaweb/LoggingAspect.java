package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingAspect {
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Pointcut("execution(* ro.unitbv.mi.javaweb.Cat.*(..))")
	public void metoda() { }
	
	
	@Before("execution(* ro.unitbv.mi.javaweb.Cat.*(..))")
	public void before(JoinPoint joinPoint) {
		log.debug("Before entering " + joinPoint.getSignature().getName() + "() method");
	}
	
	@After("metoda()")
	public void after(JoinPoint joinPoint) {
		log.debug("After entering " + joinPoint.getSignature().getName() + "() method");
	}
	
	@AfterReturning(pointcut="execution(* ro.unitbv.mi.javaweb.Cat.*(..))", returning="result")
	public void afterReturning(JoinPoint joinPoint, Object result) {
		log.debug("After returning " 
				+ joinPoint.getSignature().getName() 
				+ "() method, result is: \"" 
				+ (result instanceof String ? (String)result : "UNKNOWN")
				+ "\"");
	}
	
	@AfterThrowing("metoda()")
	public void afterReturning(JoinPoint joinPoint) {
		log.debug("After throwing " + joinPoint.getSignature().getName() + "() method");
	}
	
	@Around("execution(* ro.unitbv.mi.javaweb.Dog.feed(..))")
	public Object checkDogFeeding(ProceedingJoinPoint point) {
		log.debug("Around executing " + point.getSignature().getName() + "() method, with parameter: " + (String)point.getArgs()[0]);
		
		Dog dog = (Dog)point.getTarget();
		Object result = null;
		
		if (dog.isWild()) {
			log.warn("you cannot feed a wild dog! skipping feed()");
			return "NOTHING";
		}
		
		try {
			result = point.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		return result;
	}
}

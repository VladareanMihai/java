package ro.unitbv.mi.javaweb;

import org.springframework.stereotype.Component;

@Component
public class Hamster implements Pet {
	public String food = "grain";
	public String getFood() {
		return food;
	}
	public void setFood(String food) {
		this.food = food;
	}
	public String eats(String food){
		this.food = food;
		return "";
	}
	

}

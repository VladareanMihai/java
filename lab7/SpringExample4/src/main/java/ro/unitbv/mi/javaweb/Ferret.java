package ro.unitbv.mi.javaweb;

import org.springframework.stereotype.Component;

@Component
public class Ferret implements Pet{
	public String food = "meat";
	public String eats(String food) {
		this.food = food;
		return "";
	}
	public String getFood() {
		return food;
	}
	public void setFood(String food) {
		this.food = food;
	}
	

}

package ro.unitbv.mi.javaweb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;


@Component
public class Dog {
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	private boolean isWild = false;
	
	public boolean isWild() { return isWild; }
	public void setWild(boolean isWild) {
		this.isWild = isWild;
	}

	public String getMessage() {
		return "this is a dog";
	}

	public String feed(String food) {
		log.debug("feeding dog ..");
		return new String("the dog was fed with " + food);
	}
}

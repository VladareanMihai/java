package ro.unitbv.mi.javaweb;

import org.springframework.stereotype.Component;

@Component
public interface Pet {
	public abstract String eats(String food);
}
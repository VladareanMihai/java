package ro.unitbv.mi.javaweb;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/context.xml")
public class AppTest 
{
	private Logger log = LogManager.getLogger(this.getClass().getName());
	
	@Autowired
	private Cat cat;
	
	@Autowired
	private Dog dog;
		
    @Test
	public void testApp() {
    	log.info("message: " + cat.getMessage());
        assertTrue( true );
    }
    
    @Test
    public void testDog() {
    	log.info("message: " + dog.getMessage());
    	
    	String result = dog.feed("meat");
    	log.info("1st feed() result: " + result);
    	
    	dog.setWild(true);
    	result = dog.feed("meat");
    	log.info("2nd feed() result: " + result);    	
    }
   
    
    
    @Autowired
    private Pet hamster;
    
    @Autowired
    private Pet ferret;
    
    
    // TODO: Creati clasele Hamster si Ferret, ambele implementind interfata Pet,
    // interfata ce are o singura metoda, denumita eat().
    // Metoda va fi simpla, fara a verifica argumentul primit. In schimb, implementati un aspect
    // cu o metoda de tip @Around, ce va verifica tipul argumentului precum si tipul clasei a carei
    // metoda eat() este chemata. Daca, de exemplu, un hamster este pus sa manance carne, aspectul
    // va arunca exceptie.
    // Decomentati liniile anterioare respectiv ulterioare, astfel incat testul sa treaca.
    @Test
    public void testPets() {
    	assertTrue(hamster.eats("grain").equals("hamster eating grain"));
    	
    	String result = null;
    	try {
    		hamster.eats("meat");
    	} catch (IllegalArgumentException ex) {
    		result = ex.getMessage();
    	}
    	assertTrue(result.equals("hamster does not eat meat"));
    	
    	assertTrue(ferret.eats("meat").equals("ferret eating meat"));
    	
    	result = null;
    	try {
    		ferret.eats("grain");
    	} catch (IllegalArgumentException ex) {
    		result = ex.getMessage();
    	}
    	assertTrue(result.equals("ferret does not eat grain"));    	
    }
}
